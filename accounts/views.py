from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from accounts.forms import UserGroupForm
from django.contrib.auth.models import User, Group
from .models import Usuario


def signup(request):
    if request.method == 'POST':
        form = UserGroupForm(request.POST)
        if form.is_valid():

            user = form.save()
            user_group = Group.objects.get(name=user.type)
            user.groups.add(user_group)
            
            user_djangouser = user
            user_chavePix = form.cleaned_data['pix']
            usuario = Usuario(djangouser= user_djangouser, chavepix= user_chavePix)
            usuario.save()
            
            return HttpResponseRedirect(reverse('index'))
    else:
        form = UserGroupForm()

    context = {'form': form}
    return render(request, 'accounts/signup.html', context)