from django.db import models
from django.conf import settings
from django.db.models.deletion import CASCADE
from django.db.models.fields.related import OneToOneField
from django.contrib.auth.models import User


class Usuario(models.Model):
    djangouser = OneToOneField(User,
                               on_delete=models.CASCADE)
    chavepix = models.CharField(max_length=255)

    def __str__(self):
        return f'{self.djangouser.username}'