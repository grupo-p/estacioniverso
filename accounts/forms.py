from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.contrib.auth.models import User, Group


class UserGroupForm( UserCreationForm ):
    type = forms.ModelChoiceField(queryset=Group.objects.all(), required=True)
    pix = forms.CharField(label = "Chave Pix")

    def save(self, commit=True):
        user = super(UserGroupForm, self).save(commit=False)
        user.type = self.cleaned_data["type"]
        user.pix = self.cleaned_data["pix"]

        if commit:
            user.save()
        return user