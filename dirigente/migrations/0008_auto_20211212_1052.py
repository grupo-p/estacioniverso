# Generated by Django 3.2.8 on 2021-12-12 13:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dirigente', '0007_auto_20211211_1815'),
    ]

    operations = [
        migrations.AddField(
            model_name='solicitacaoentrada',
            name='recusada',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='solicitacaosaida',
            name='recusada',
            field=models.BooleanField(default=False),
        ),
    ]
