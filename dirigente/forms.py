from django.forms import ModelForm
from .models import Estacionamento

class EstacionamentoForm(ModelForm):
    class Meta:
        model = Estacionamento
        fields = [
            'nome',
            'endereco',
            'preco1h',
            'precoHoraAdicional',
            'precoDiaria',
            'vagasCarro',
            'vagasMoto'
        ]
        labels = {
            'nome': "Nome",
            'endereco': "Endereço",
            'preco1h': "Preço da primeira hora",
            'precoHoraAdicional': "Preço por hora adicional",
            'precoDiaria': "Preço da diária",
            'vagasCarro': "Número de vagas para carro",
            'vagasMoto': "Número de vagas para motocicletas",
        }