from django.apps import AppConfig


class DirigenteConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'dirigente'
