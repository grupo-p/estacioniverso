from django.db import models
from django.utils import timezone
from django.conf import settings
from django.db.models.deletion import CASCADE, SET_NULL
from django.db.models.fields.related import OneToOneField
from django.contrib.auth.models import User
from motorista.models import Veiculo
from accounts.models import Usuario

class TipoVaga(models.Model):
    desc=models.CharField(max_length=255)

    def __str__(self):
        return f'{self.desc}'

class Estacionamento(models.Model):
    dono = OneToOneField(User, on_delete=models.CASCADE)
    nome = models.CharField(max_length=255)
    endereco = models.CharField(max_length=255)
    aberto = models.BooleanField(default=False)
    preco1h = models.FloatField()
    precoHoraAdicional = models.FloatField()
    precoDiaria = models.FloatField()
    vagasCarro = models.IntegerField()
    vagasMoto = models.IntegerField()

    def calcular_ocupacao(self):
        vagasocupadas = self.vaga_set.filter(ocupada=True)

        vagascarro = self.vaga_set.filter(tipo=1)
        numvagascarro = vagascarro.count()

        if numvagascarro:

            vagasocupadascarro = vagasocupadas.filter(tipo=1)
            numvagasocupadascarro = vagasocupadascarro.count()

            ocupacaocarro = 100*numvagasocupadascarro/numvagascarro
        
        else:
            ocupacaocarro = 100

        vagasmoto = self.vaga_set.filter(tipo=2)
        numvagasmoto = vagasmoto.count()

        if numvagasmoto:

            vagasocupadasmoto = vagasocupadas.filter(tipo=2)
            numvagasocupadasmoto = vagasocupadasmoto.count()

            ocupacaomoto = 100*numvagasocupadasmoto/numvagasmoto
        
        else:
            ocupacaomoto = 100

        return (ocupacaocarro, ocupacaomoto)

    def __str__(self):
        return f'{self.nome}'

class Vaga(models.Model):
    numero = models.IntegerField()
    estacionamento = models.ForeignKey(Estacionamento, on_delete=CASCADE)
    tipo = models.ForeignKey(TipoVaga, on_delete=CASCADE)
    ocupada=models.BooleanField(default=False)

    def __str__(self):
        return f'{self.estacionamento.nome} - {self.numero}'

class Solicitacao(models.Model):
    estacionamento = models.ForeignKey(Estacionamento, on_delete=CASCADE)
    veiculo = models.ForeignKey(Veiculo, on_delete=CASCADE)

    def __str__(self):
        return f'{self.estacionamento.nome} - {self.veiculo.marca} - {self.veiculo.modelo}'

class SolicitacaoEntrada(models.Model):
    solicitacao = models.OneToOneField(Solicitacao, on_delete=CASCADE)
    aceita = models.BooleanField(default=False)
    horaAceitacao = models.DateTimeField(auto_now=True)
    recusada = models.BooleanField(default=False)
    
    def __str__(self):
        return f'{self.solicitacao}'

class SolicitacaoSaida(models.Model):
    solicitacao = models.OneToOneField(Solicitacao, on_delete=CASCADE)
    aceita = models.BooleanField(default=False)
    horaAceitacao = models.DateTimeField(auto_now=True)
    recusada = models.BooleanField(default=False)
    
    def __str__(self):
        return f'{self.solicitacao}'

class Ocupacao(models.Model):
    veiculo = models.ForeignKey(Veiculo, on_delete=CASCADE)
    vaga = models.ForeignKey(Vaga, on_delete=CASCADE)
    solicitacaoentrada = models.OneToOneField(SolicitacaoEntrada, on_delete=CASCADE)
    solicitacaosaida = models.OneToOneField(SolicitacaoSaida, on_delete=SET_NULL, blank=True, null=True)
    ativa = models.BooleanField(default=True)
    
    def __str__(self):
        return f'{self.veiculo} - {self.vaga}'

class Pagamento(models.Model):
    motorista = models.ForeignKey(Usuario,
                               on_delete=CASCADE, related_name='pagamentos')
    dirigente = models.ForeignKey(Usuario,
                               on_delete=CASCADE, related_name='recebimentos')
    ocupacao = models.OneToOneField(Ocupacao, on_delete=CASCADE)
    valor = models.FloatField(blank=True, null=True)
    concluido = models.BooleanField(default=False)

    def calcular_valor(self):
        estacionamento = self.ocupacao.vaga.estacionamento
        tempo = (self.ocupacao.solicitacaosaida.horaAceitacao-self.ocupacao.solicitacaoentrada.horaAceitacao).seconds
        numhoras = (tempo // 3600)
        numdias = numhoras // 24
        numhoras = numhoras - 24*numdias

        self.valor = estacionamento.preco1h + estacionamento.precoHoraAdicional*numhoras + estacionamento.precoDiaria*numdias

        return self.valor

    def __str__(self):
        return f'{self.motorista} -> {self.dirigente} - R${self.valor}'