from django.contrib import admin

from .models import *

admin.site.register(TipoVaga)
admin.site.register(Estacionamento)
admin.site.register(Vaga)
admin.site.register(Solicitacao)
admin.site.register(SolicitacaoEntrada)
admin.site.register(SolicitacaoSaida)
admin.site.register(Ocupacao)
admin.site.register(Pagamento)
