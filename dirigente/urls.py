from django.urls import path

from . import views

app_name = 'dirigente'
urlpatterns = [
    path('estacionamento/create/', views.create_estacionamento, name='create'),
    path('estacionamento', views.detail_estacionamento, name='detail'), 
    path('estacionamento/atualizar', views.atualizar_estacionamento, name='atualizar'),
    path('estacionamento/delete/', views.delete_estacionamento, name='delete'),
    path('estacionamento/abrir', views.abrir_estacionamento, name='abrir'),
    path('estacionamento/fechar', views.fechar_estacionamento, name='fechar'),
    path('estacionamento/aceitarentrada/<int:solicitacao_id>', views.aceitar_entrada, name='aceitarentrada'),
    path('estacionamento/recusarentrada/<int:solicitacao_id>', views.recusar_entrada, name='recusarentrada'),
    path('estacionamento/aceitarsaida/<int:solicitacao_id>', views.aceitar_saida, name='aceitarsaida'),
    path('estacionamento/recusarsaida/<int:solicitacao_id>', views.recusar_saida, name='recusarsaida'),
]