from django.db.models.fields import NullBooleanField
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from .models import *
from .forms import *

def detail_estacionamento(request):
    ocupacaocarro = None
    ocupacaomoto = None
    lista_ocupacao = None
    lista_pagamentos = None
    lista_solicitacoes_entrada = None
    lista_solicitacoes_saida = None
    valor_total = None
    try:
        estacionamento = request.user.estacionamento

        ocupacaocarro, ocupacaomoto = estacionamento.calcular_ocupacao()

        lista_ocupacao = []

        lista_vagas = request.user.estacionamento.vaga_set.filter(ocupada=True)

        lista_pagamentos= request.user.usuario.recebimentos.filter(concluido=True)
        valor_total = 0

        lista_solicitacoes = estacionamento.solicitacao_set.all()
        lista_solicitacoes_entrada = []
        lista_solicitacoes_saida = []

        for vaga in lista_vagas:
            lista_ocupacao.append(vaga.ocupacao_set.filter(ativa=True)[0])
        
        for pagamento in lista_pagamentos:
            valor_total += pagamento.valor

        for solicitacao in lista_solicitacoes:
            if solicitacao.solicitacaoentrada.aceita == False and solicitacao.solicitacaoentrada.recusada == False:
                lista_solicitacoes_entrada.append(solicitacao.solicitacaoentrada)

        try:
            if solicitacao.solicitacaosaida.aceita == False and solicitacao.solicitacaosaida.recusada == False:
                lista_solicitacoes_saida.append(solicitacao.solicitacaosaida)
        except:
            pass
    except:
        estacionamento = None
    
    context = {'estacionamento': estacionamento,
                'ocupacaocarro': ocupacaocarro,
                'ocupacaomoto': ocupacaomoto,
                'lista_ocupacao': lista_ocupacao,
                'lista_pagamentos': lista_pagamentos,
                'lista_solicitacoes_saida': lista_solicitacoes_saida,
                'lista_solicitacoes_entrada': lista_solicitacoes_entrada,
                'valor_total':valor_total
                }
    return render(request, 'estacionamento/detail.html', context)

def create_estacionamento(request):
    if request.method == 'POST':
        form = EstacionamentoForm(request.POST)
        if form.is_valid():
            estacionamento_dono = request.user
            estacionamento_nome = form.cleaned_data['nome']
            estacionamento_endereco = form.cleaned_data['endereco']
            estacionamento_preco1h = form.cleaned_data['preco1h']
            estacionamento_precoHoraAdicional = form.cleaned_data['precoHoraAdicional']
            estacionamento_precoDiaria = form.cleaned_data['precoDiaria']
            estacionamento_vagasCarro = form.cleaned_data['vagasCarro']
            estacionamento_vagasMoto = form.cleaned_data['vagasMoto']

            estacionamento = Estacionamento(nome=estacionamento_nome, endereco=estacionamento_endereco,
                                preco1h = estacionamento_preco1h, precoHoraAdicional = estacionamento_precoHoraAdicional,
                                precoDiaria = estacionamento_precoDiaria, dono=estacionamento_dono,
                                vagasCarro = estacionamento_vagasCarro, vagasMoto=estacionamento_vagasMoto)
            estacionamento.save()

            if estacionamento_vagasCarro:
                for i in range(estacionamento_vagasCarro):
                    vaga = Vaga(numero = i+1, estacionamento=estacionamento, tipo=TipoVaga.objects.get(desc="Carro"))
                    vaga.save()

            if estacionamento_vagasMoto:
                for i in range(estacionamento_vagasMoto):
                    vaga = Vaga(numero = i+estacionamento_vagasCarro+1, estacionamento=estacionamento, tipo=TipoVaga.objects.get(desc="Moto"))
                    vaga.save()

            return HttpResponseRedirect(
                reverse('dirigente:detail'))
    else:
        form = EstacionamentoForm()
    context = {'form': form}
    return render(request, 'estacionamento/create.html', context)

def atualizar_estacionamento(request):
    return HttpResponseRedirect(
                reverse('dirigente:detail'))

def aceitar_entrada(request, solicitacao_id):
    solicitacao = get_object_or_404(SolicitacaoEntrada, pk=solicitacao_id)
    estacionamento = request.user.estacionamento

    if solicitacao.solicitacao.veiculo.tipo.desc == "Carro":
        vaga = estacionamento.vaga_set.filter(ocupada=False, tipo=1)[0]
    if solicitacao.solicitacao.veiculo.tipo.desc == "Moto":
        vaga = estacionamento.vaga_set.filter(ocupada=False, tipo=2)[0]

    solicitacao.aceita = True
    solicitacao.save()

    ocupacao = Ocupacao(veiculo=solicitacao.solicitacao.veiculo, vaga=vaga, 
                        solicitacaoentrada=solicitacao,
                        ativa=True)

    ocupacao.save()
    vaga.ocupada = True
    vaga.save()

    return HttpResponseRedirect(
        reverse('dirigente:detail'))
    
def recusar_entrada(request, solicitacao_id):
    solicitacao = get_object_or_404(SolicitacaoEntrada, pk=solicitacao_id)

    solicitacao.recusada = True
    solicitacao.save()

    return HttpResponseRedirect(
        reverse('dirigente:detail'))

def aceitar_saida(request,solicitacao_id, ):
    solicitacao = get_object_or_404(SolicitacaoSaida, pk=solicitacao_id)
    ocupacao = solicitacao.ocupacao
    vaga = ocupacao.vaga
    pagamento = ocupacao.pagamento

    solicitacao.aceita = True
    solicitacao.save()

    ocupacao.ativa = False
    ocupacao.save()

    pagamento.concluido = True
    pagamento.save()

    vaga.ocupada = False
    vaga.save()

    return HttpResponseRedirect(
        reverse('dirigente:detail'))

def recusar_saida(request,solicitacao_id):
    solicitacao = get_object_or_404(SolicitacaoSaida, pk=solicitacao_id)

    solicitacao.recusada = True
    solicitacao.save()

    return HttpResponseRedirect(
        reverse('dirigente:detail'))

def abrir_estacionamento(request):
    estacionamento = request.user.estacionamento
    estacionamento.aberto = True
    estacionamento.save()

    return HttpResponseRedirect(
                reverse('dirigente:detail'))

def fechar_estacionamento(request):
    estacionamento = request.user.estacionamento
    estacionamento.aberto = False
    estacionamento.save()

    return HttpResponseRedirect(
                reverse('dirigente:detail'))

def delete_estacionamento(request):
    estacionamento = request.user.estacionamento

    if request.method == "POST":
        estacionamento.delete()
        return HttpResponseRedirect(reverse('index'))

    context = {'estacionamento': estacionamento}
    return render(request, 'estacionamento/delete.html', context)
