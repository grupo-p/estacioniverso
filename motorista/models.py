from django.db import models
from django.conf import settings
from django.db.models.deletion import CASCADE

class Cor(models.Model):
    desc=models.CharField(max_length=255)

    def __str__(self):
        return f'{self.desc}'

class Marca(models.Model):
    desc=models.CharField(max_length=255)

    def __str__(self):
        return f'{self.desc}'

class TipoVeiculo(models.Model):
    desc=models.CharField(max_length=255)

    def __str__(self):
        return f'{self.desc}'

class Veiculo(models.Model):
    dono = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    marca = models.ForeignKey(Marca, on_delete= CASCADE)
    modelo = models.CharField(max_length=255)
    placa = models.CharField(max_length=7)
    cor = models.ForeignKey(Cor, on_delete= CASCADE)
    tipo = models.ForeignKey(TipoVeiculo, on_delete= CASCADE)

    def __str__(self):
        return f'{self.marca} - {self.modelo} - {self.placa} - {self.dono}'
