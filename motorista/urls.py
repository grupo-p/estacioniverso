from django.urls import path

from . import views

app_name = 'motorista'
urlpatterns = [
    path('veiculos', views.list_veiculo, name='veiculos'),
    path('veiculos/create/', views.create_veiculo, name='create'),
    path('veiculos/<int:veiculo_id>/', views.detail_veiculo, name='detail'), 
    path('veiculos/delete/<int:veiculo_id>/', views.delete_veiculo, name='delete'),

    path('estacionamentos', views.list_estacionamento, name='estacionamentos'),
    path('estacionamentos/<int:estacionamento_id>/', views.detail_estacionamento, name='detailestacionamento'),

    path('estacionamentos/<int:estacionamento_id>/entrar', views.entrar_estacionamento, name='entrar'), 
    path('estacionamentos/<int:estacionamento_id>/entrar/<int:solicitacao_id>', views.aguardar_entrada, name='aguardoentrada' ),
    path('estacionamentos/<int:estacionamento_id>/entrar/<int:solicitacao_id>/update', views.aguardar_entrada_update, name='aguardoentradaupdate' ),
    path('estacionamentos/<int:estacionamento_id>/entrar/<int:solicitacao_id>/recusada', views.entrada_recusada, name='entradarecusada'),

    path('estacionamentos/<int:estacionamento_id>/entrar/lotado', views.estacionamento_lotado, name='lotado'),

    path('estacionamentos/<int:estacionamento_id>/sair/<int:solicitacao_id>/<int:ocupacao_id>', views.sair_estacionamento, name='sair'),
    path('estacionamentos/<int:estacionamento_id>/sair/<int:solicitacao_id>/<int:ocupacao_id>/<int:solicitacaosaida_id>/<int:pagamento_id>', views.aguardar_saida, name='aguardosaida' ),
    path('estacionamentos/<int:estacionamento_id>/sair/<int:solicitacao_id>/<int:ocupacao_id>/<int:solicitacaosaida_id>/update', views.aguardar_saida_update, name='aguardosaidaupdate' ),
    path('estacionamentos/<int:estacionamento_id>/sair/<int:solicitacao_id>/<int:ocupacao_id>/<int:solicitacaosaida_id>/recusada', views.saida_recusada, name='saidarecusada'),

]