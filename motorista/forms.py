from django.forms import ModelForm
from django import forms
from dirigente.models import Solicitacao
from .models import Veiculo

class VeiculoForm(ModelForm):
    class Meta:
        model = Veiculo
        fields = [
            'marca',
            'modelo',
            'placa',
            'cor',
            'tipo'
        ]
        labels = {
            'marca': "Marca",
            'modelo': "Modelo",
            'placa': "Placa",
            'cor': "Cor",
            'tipo': "Tipo"
        }

class SolicitacaoEntradaForm(forms.Form):
    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(SolicitacaoEntradaForm, self).__init__(*args, **kwargs)
        self.fields['veiculo'] = forms.ModelChoiceField(queryset=user.veiculo_set.all(), required=True)
