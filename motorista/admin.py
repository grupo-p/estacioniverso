from django.contrib import admin

from .models import Cor, Marca, TipoVeiculo, Veiculo

admin.site.register(Cor)
admin.site.register(Marca)
admin.site.register(TipoVeiculo)
admin.site.register(Veiculo)