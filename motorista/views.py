from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from .models import *
from dirigente.models import *
from .forms import *

def list_veiculo(request):
    veiculo_list = request.user.veiculo_set.all()
    context = {'veiculo_list': veiculo_list}
    return render(request, 'veiculos/lista.html', context)

def detail_veiculo(request, veiculo_id):
    veiculo = get_object_or_404(Veiculo, pk=veiculo_id)
    context = {'veiculo': veiculo}
    return render(request, 'veiculos/detail.html', context)

def create_veiculo(request):
    if request.method == 'POST':
        form = VeiculoForm(request.POST)
        if form.is_valid():
            veiculo_dono = request.user
            veiculo_marca = form.cleaned_data['marca']
            veiculo_modelo = form.cleaned_data['modelo']
            veiculo_placa = form.cleaned_data['placa']
            veiculo_cor = form.cleaned_data['cor']
            veiculo_tipo = form.cleaned_data['tipo']

            veiculo = Veiculo(marca=veiculo_marca, modelo=veiculo_modelo,
                                placa = veiculo_placa, cor = veiculo_cor,
                                tipo = veiculo_tipo, dono=veiculo_dono)
            veiculo.save()
            return HttpResponseRedirect(
                reverse('motorista:detail', args=(veiculo.id, )))
    else:
        form = VeiculoForm()
    context = {'form': form}
    return render(request, 'veiculos/create.html', context)

def delete_veiculo(request, veiculo_id):
    veiculo = get_object_or_404(Veiculo, pk=veiculo_id)

    if request.method == "POST":
        veiculo.delete()
        return HttpResponseRedirect(reverse('motorista:veiculos'))

    context = {'veiculo': veiculo}
    return render(request, 'veiculos/delete.html', context)

def list_estacionamento(request):
    estacionamento_list = Estacionamento.objects.filter(aberto=True)
    ocupacao_carro_list = []
    ocupacao_moto_list = []

    for estacionamento in estacionamento_list:
        ocupacaocarro, ocupacaomoto = estacionamento.calcular_ocupacao()

        ocupacao_carro_list.append(ocupacaocarro)
        ocupacao_moto_list.append(ocupacaomoto)

    ha_estacionamento = bool(estacionamento_list)
    estacionamento_list = zip(estacionamento_list, ocupacao_carro_list, ocupacao_moto_list)

    context = {'estacionamento_list': estacionamento_list,
                'ha_estacionamentos': ha_estacionamento}

    return render(request, 'estacionamentos/lista.html', context)

def detail_estacionamento(request, estacionamento_id):
    estacionamento = get_object_or_404(Estacionamento, pk=estacionamento_id)
    ocupacaocarro, ocupacaomoto = estacionamento.calcular_ocupacao()
    context = {'estacionamento': estacionamento,
                'ocupacaocarro': ocupacaocarro,
                'ocupacaomoto': ocupacaomoto}

    return render(request, 'estacionamentos/detail.html', context)

def entrar_estacionamento(request, estacionamento_id):
    estacionamento = get_object_or_404(Estacionamento, pk=estacionamento_id)

    if request.method == 'POST':
        form = SolicitacaoEntradaForm(request.user, request.POST)
        if form.is_valid():
            solicitacao_veiculo = form.cleaned_data['veiculo']
            ocupacaocarro, ocupacaomoto = estacionamento.calcular_ocupacao()

            if solicitacao_veiculo.tipo.desc == "Carro" and ocupacaocarro == 100:
                return HttpResponseRedirect(reverse('motorista:lotado', args=(estacionamento.id, )))
                
            elif solicitacao_veiculo.tipo.desc == "Moto" and ocupacaomoto == 100:
                return HttpResponseRedirect(reverse('motorista:lotado', args=(estacionamento.id, )))

            solicitacao = Solicitacao(estacionamento = estacionamento, veiculo=solicitacao_veiculo)
            solicitacao.save()
            solicitacaoentrada = SolicitacaoEntrada(solicitacao=solicitacao)
            solicitacaoentrada.save()

            return HttpResponseRedirect(
                reverse('motorista:aguardoentrada', args=(estacionamento.id, solicitacao.id)))

    else:
        form = SolicitacaoEntradaForm(user=request.user)

    context = {'form': form}
    return render(request, 'estacionamentos/entrar.html', context)

def aguardar_entrada(request, estacionamento_id, solicitacao_id):
    estacionamento = get_object_or_404(Estacionamento, pk=estacionamento_id)
    solicitacao = get_object_or_404(Solicitacao, pk=solicitacao_id)
    
    context ={
        "tipo_solicitacao": "Entrada",
        'estacionamento': estacionamento,
        'solicitacao': solicitacao
    }
    return render(request, 'estacionamentos/aguardo.html', context)

def aguardar_entrada_update (request, estacionamento_id, solicitacao_id):
    estacionamento = get_object_or_404(Estacionamento, pk=estacionamento_id)
    solicitacao = get_object_or_404(Solicitacao, pk=solicitacao_id)
    solicitacaoentrada = solicitacao.solicitacaoentrada

    if solicitacaoentrada.aceita == True:
        ocupacao = solicitacaoentrada.ocupacao
        return HttpResponseRedirect(reverse('motorista:sair', args=(estacionamento.id, solicitacao.id, ocupacao.id)))

    if solicitacaoentrada.recusada == True:
        return HttpResponseRedirect(reverse('motorista:entradarecusada', args=(estacionamento.id, solicitacao.id)))
    
    context ={
        "tipo_solicitacao": "Entrada",
        'estacionamento': estacionamento,
        'solicitacao': solicitacao
    }
    return render(request, 'estacionamentos/aguardo.html', context)

def sair_estacionamento(request, estacionamento_id, solicitacao_id, ocupacao_id):
    estacionamento = get_object_or_404(Estacionamento, pk=estacionamento_id)
    solicitacao = get_object_or_404(Solicitacao, pk=solicitacao_id)
    ocupacao = get_object_or_404(Ocupacao, pk=ocupacao_id)

    if request.method == 'POST':
        solicitacaosaida = SolicitacaoSaida(solicitacao=solicitacao)
        solicitacaosaida.save()
        ocupacao.solicitacaosaida = solicitacaosaida
        ocupacao.save()
        pagamento = Pagamento(motorista = request.user.usuario, dirigente=estacionamento.dono.usuario,
                                ocupacao = ocupacao)

        pagamento.calcular_valor()
        pagamento.save()

        return HttpResponseRedirect(reverse('motorista:aguardosaida', args=(estacionamento.id, solicitacao.id, ocupacao.id, solicitacaosaida.id, pagamento.id)))
    else:
        context = {'vaga': ocupacao.vaga}
        return render(request, 'estacionamentos/sair.html', context)

def aguardar_saida(request, estacionamento_id, solicitacao_id, ocupacao_id, solicitacaosaida_id, pagamento_id):
    estacionamento = get_object_or_404(Estacionamento, pk=estacionamento_id)
    solicitacao = get_object_or_404(Solicitacao, pk=solicitacao_id)
    ocupacao = get_object_or_404(Ocupacao, pk=ocupacao_id)
    solicitacaosaida = get_object_or_404(SolicitacaoSaida, pk=solicitacaosaida_id)
    pagamento = get_object_or_404(Pagamento, pk=pagamento_id)
    
    context ={
        "tipo_solicitacao": "Saida",
        'estacionamento': estacionamento,
        'solicitacao': solicitacao,
        'ocupacao': ocupacao,
        "solicitacaosaida": solicitacaosaida,
        "pagamento" : pagamento,
    }
    return render(request, 'estacionamentos/aguardo.html', context)

def aguardar_saida_update (request, estacionamento_id, solicitacao_id, ocupacao_id, solicitacaosaida_id):
    estacionamento = get_object_or_404(Estacionamento, pk=estacionamento_id)
    solicitacao = get_object_or_404(Solicitacao, pk=solicitacao_id)
    solicitacaosaida = get_object_or_404(SolicitacaoSaida, pk=solicitacaosaida_id)
    ocupacao = get_object_or_404(Ocupacao, pk=ocupacao_id)
    pagamento = ocupacao.pagamento
    
    if solicitacaosaida.aceita == True:
        return HttpResponseRedirect(reverse('motorista:detailestacionamento', args=(estacionamento.id, )))

    if solicitacaosaida.recusada == True:
        return HttpResponseRedirect(reverse('motorista:saidarecusada', args=( estacionamento.id, solicitacao.id, ocupacao.id, solicitacaosaida_id)))
    
    context ={
        "tipo_solicitacao": "Saida",
        'estacionamento': estacionamento,
        'solicitacao': solicitacao,
        'ocupacao': ocupacao,
        "solicitacaosaida": solicitacaosaida,
        "pagamento" : pagamento,
    }
    return render(request, 'estacionamentos/aguardo.html', context)

def entrada_recusada(request, estacionamento_id, solicitacao_id):
    estacionamento = get_object_or_404(Estacionamento, pk=estacionamento_id)
    solicitacao = get_object_or_404(Solicitacao, pk=solicitacao_id)

    solicitacao.delete()

    context ={
        "tipo_solicitacao": "Entrada",
        'estacionamento': estacionamento,
    }

    return render(request, 'estacionamentos/recusada.html', context)

def estacionamento_lotado(request, estacionamento_id):
    estacionamento = get_object_or_404(Estacionamento, pk=estacionamento_id)

    context ={
        'estacionamento': estacionamento,
    }

    return render(request, 'estacionamentos/lotado.html', context)

def saida_recusada(request, estacionamento_id, solicitacao_id, ocupacao_id, solicitacaosaida_id):
    estacionamento = get_object_or_404(Estacionamento, pk=estacionamento_id)
    solicitacao = get_object_or_404(Solicitacao, pk=solicitacao_id)
    solicitacaosaida = get_object_or_404(SolicitacaoSaida, pk=solicitacaosaida_id)
    ocupacao = get_object_or_404(Ocupacao, pk=ocupacao_id)

    solicitacaosaida.delete()
    ocupacao.pagamento.delete()

    context ={
        "tipo_solicitacao": "Saida",
        'estacionamento': estacionamento,
        'solicitacao': solicitacao,
        'ocupacao':ocupacao
    }

    return render(request, 'estacionamentos/recusada.html', context)