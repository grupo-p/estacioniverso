from rest_framework import serializers

from accounts.models import *
from dirigente.models import *
from motorista.models import *



class UsuarioSerializer(serializers.ModelSerializer):

    class Meta:
        model = Usuario
        fields = ['djangouser', 'chavepix']


class EstacionamentoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Estacionamento
        fields = ['dono', 'nome','endereco', 'aberto', 'preco1h', 'precoHoraAdicional', 'precoDiaria', 'vagasCarro', 'vagasMoto']

class VagaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Vaga
        fields = ['numero', 'estacionamento','tipo', 'ocupada']

class OcupacaoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Ocupacao
        fields = ['veiculo', 'vaga', 'solicitacaoentrada', 'solicitacaosaida', 'ativa']

class PagamentoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Pagamento
        fields = ['motorista', 'dirigente', 'ocupacao', 'valor', 'concluido']

