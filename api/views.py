from rest_framework import generics

from accounts.models import *
from dirigente.models import *
from motorista.models import *
from .serializers import *


class UsuarioList(generics.ListAPIView):
    queryset = Usuario.objects.all()
    serializer_class = UsuarioSerializer


class UsuarioDetail(generics.RetrieveAPIView):
    queryset = Usuario.objects.all()
    serializer_class = UsuarioSerializer

class EstacionamentoList(generics.ListAPIView):
    queryset = Estacionamento.objects.all()
    serializer_class = EstacionamentoSerializer
    
class EstacionamentoDetail(generics.RetrieveAPIView):
    queryset = Estacionamento.objects.all()
    serializer_class = EstacionamentoSerializer

class VagaList(generics.ListAPIView):
    queryset = Vaga.objects.all()
    serializer_class = VagaSerializer
    
class VagaDetail(generics.RetrieveAPIView):
    queryset = Vaga.objects.all()
    serializer_class = VagaSerializer

class PagamentoList(generics.ListAPIView):
    queryset = Pagamento.objects.all()
    serializer_class = PagamentoSerializer
    
class PagamentoDetail(generics.RetrieveAPIView):
    queryset = Pagamento.objects.all()
    serializer_class = PagamentoSerializer

class OcupacaoList(generics.ListAPIView):
    queryset = Ocupacao.objects.all()
    serializer_class = OcupacaoSerializer
    
class OcupacaoDetail(generics.RetrieveAPIView):
    queryset = Ocupacao.objects.all()
    serializer_class = OcupacaoSerializer