from django.urls import path
from .views import *

urlpatterns = [
    path('usuarios/<int:pk>/', UsuarioDetail.as_view()),
    path('usuarios/', UsuarioList.as_view()),
    path('estacionamentos/<int:pk>/', EstacionamentoDetail.as_view()),
    path('estacionamentos/', EstacionamentoList.as_view()),
    path('vaga/<int:pk>/', VagaDetail.as_view()),
    path('vaga/', VagaList.as_view()),
    path('ocupacao/<int:pk>/', OcupacaoDetail.as_view()),
    path('ocupacao/', OcupacaoList.as_view()),
    path('pagamento/<int:pk>/', PagamentoDetail.as_view()),
    path('pagamento/', PagamentoList.as_view()),
]